'use strict'

###*
 # @ngdoc service
 # @name crucibleApp.setting
 # @description
 # # setting
 # Service in the crucibleApp.
###
angular.module 'crucibleApp'
  .service 'settingStorage', ->
    defaults =
      url : "http://localhost:8060/"
      cruUrl : "http://localhost:8060/cru/"

    service =
      init: (callback) ->
        service.get( (result)->
          if (result.setting)
            service.setting = result.setting
          else
            service.setting = defaults

          service.save(callback)
        )

      get: (callback) ->
        chrome.storage.local.get('setting', callback)

      restore: () ->
        service.setting = defaults

      save: (callback) ->
        chrome.storage.local.set({'setting': service.setting},callback);

      getConfig: () ->
        service.setting

    return service

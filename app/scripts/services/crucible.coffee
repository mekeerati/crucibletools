'use strict'

###*
 # @ngdoc service
 # @name crucibleApp.crucible
 # @description
 # # crucible
 # Service in the crucibleApp.
###
angular.module 'crucibleApp'
  .service 'crucible', ($http,settingStorage) ->
    service = {
      get: (config) -> $http.get("#{settingStorage.getConfig().url}rest-service/reviews-v1/#{config.reviewId}/comments")
    }


    return service;

'use strict'

###*
 # @ngdoc function
 # @name crucibleApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the crucibleApp
###
angular.module 'crucibleApp'
.controller 'MainCtrl', ($scope, crucible, $rootScope,settingStorage,$mdToast) ->
  settingStorage.init( ->

    $scope.setting = settingStorage.getConfig()

    console.log("complete",$scope.setting)

    $scope.reviewId = "POL-1,POL-2,POL-3"

    $scope.save = () ->
      settingStorage.save( ->
        $mdToast.show($mdToast.simple().content('Save Complete!').hideDelay(3000))
      )

    $scope.searchReview = ->
      $scope.map = {}
      config = settingStorage.getConfig()
      if(config.reviewId)
        for reviewId in config.reviewId.split(",")
          do (reviewId) ->
            crucible.get({reviewId: reviewId, url: config.url}).then((response)->
              data = response.data

              assignIsDone(data.comments)

              $scope.map[reviewId] = data
            )

    $scope.$apply()

    assignIsDone = (comments) ->
      for comment in comments
        do (comment) ->
          comment.hasChild = false

          if(comment.replies)
            comment.hasChild = true
            assignIsDone(comment.replies)

          comment.isDone = () ->
            doneList = settingStorage.getConfig().done
            if(doneList)
              for done in doneList.split(",")
                done = done.trim()
                if(done && this.message.indexOf(done) != -1 )
                  return true

              return false


  )

'use strict'

###*
 # @ngdoc function
 # @name crucibleApp.controller:AboutCtrl
 # @description
 # # AboutCtrl
 # Controller of the crucibleApp
###
angular.module 'crucibleApp'
  .controller 'AboutCtrl', ->
    @awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
    return

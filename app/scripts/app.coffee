'use strict'

###*
 # @ngdoc overview
 # @name crucibleApp
 # @description
 # # crucibleApp
 #
 # Main module of the application.
###
angular
  .module 'crucibleApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch','ngMaterial'
  ]
  .config ($routeProvider,$resourceProvider,$compileProvider) ->
    $resourceProvider.defaults.stripTrailingSlashes = false

#    fix chrome app issue
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|chrome-extension):/)

    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
        controllerAs: 'main'
      .when '/about',
        templateUrl: 'views/about.html'
        controller: 'AboutCtrl'
        controllerAs: 'about'
      .otherwise
        redirectTo: '/'


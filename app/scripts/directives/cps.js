angular.module('crucibleApp')
  .directive('cspSrc', function () {
    return {
      restrict: 'A',
      replace: false,
      priority: 99, //after all build-in directive are compiled
      link: function (scope, element, attrs) {
        attrs.$observe('cspSrc', function (value) {
          if (!value)
            return;

          if (element[0].nodeName.toLowerCase() === 'img' && value.indexOf('blob') !== 0) {
            //if it is img tag then use XHR to load image.
            var localSrc = null;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', value, true);
            xhr.responseType = 'blob';
            xhr.onload = function (e) {
              localSrc = URL.createObjectURL(this.response);
              element[0].src = localSrc;

            };
            xhr.send();
            scope.$on("$destroy", function () {
              if (localSrc) {
                URL.revokeObjectURL(localSrc);
              }
            });


          } else {
            attrs.$set('src', value);
          }
        });
      }
    };
  });

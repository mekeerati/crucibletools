'use strict'

###*
 # @ngdoc directive
 # @name crucibleApp.directive:comment
 # @description
 # # comment
###
angular.module 'crucibleApp'
  .directive 'comment', (RecursionHelper) ->
    restrict: 'EA'
    scope: {
      list: '=value'
      review: '='
      done: '='
    }
    templateUrl: 'directives/comment-directive.html'
    compile: (element,scope) ->
      return RecursionHelper.compile(element)

    controller: ($scope,settingStorage) ->
      $scope.setting = settingStorage.getConfig()

      $scope.getCommentLink = (comment) ->
        commentId = comment.permaId.id || comment.permaId
        id = commentId.split(":")[1]
        return "c##{id}"

      $scope.isAllDone = (comment) ->
        if(comment instanceof Array)
          commentList = comment
          for c in commentList
            if($scope.isAllDone(c))
              return true
        else
          if(comment.isDone())
            return true
          else if(comment.hasChild)
            return $scope.isAllDone(comment.replies)
          else
            false




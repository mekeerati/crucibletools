'use strict'

describe 'Service: crucible', ->

  # load the service's module
  beforeEach module 'crucibleApp'

  # instantiate service
  crucible = {}
  beforeEach inject (_crucible_) ->
    crucible = _crucible_

  it 'should do something', ->
    expect(!!crucible).toBe true

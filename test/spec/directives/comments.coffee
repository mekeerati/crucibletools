'use strict'

describe 'Directive: comments', ->

  # load the directive's module
  beforeEach module 'crucibleApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<comments></comments>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the comments directive'

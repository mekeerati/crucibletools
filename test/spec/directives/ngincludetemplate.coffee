'use strict'

describe 'Directive: ngIncludeTemplate', ->

  # load the directive's module
  beforeEach module 'crucibleApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<ng-include-template></ng-include-template>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the ngIncludeTemplate directive'

'use strict'

describe 'Directive: comment', ->

  # load the directive's module
  beforeEach module 'crucibleApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<comment></comment>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the comment directive'
